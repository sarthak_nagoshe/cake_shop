function customize(){
    var cakename = document.getElementById("name")
    var totalcost = document.getElementById("price")
    var vanilla = document.getElementById("vanilla");
    var chocolate = document.getElementById("chocolate");
    var choco = document.getElementById("choco-chips");
    var Mixedberries = document.getElementById("Mixed-Berries");
    var ButterCream = document.getElementById("Butter-Cream");
    var WhippedCream = document.getElementById("Whipped-Cream")
    var Lavender = document.getElementById("Lavender")
    var ItalianMeriangue = document.getElementById("ItalianMeriangue")
    var Sprinkles = document.getElementById("Sprinkles")
    var rosepetal = document.getElementById("rosepetal")
    var Candies = document.getElementById("Candies")
    var RoyalIcing = document.getElementById("RoyalIcing")
    var Butterscotch = document.getElementById("Butterscotch")
    var FreshFruits = document.getElementById("FreshFruits")
    var Frosting = document.getElementById("Frosting")
    var Lemon = document.getElementById("Lemon")
    var Caramel = document.getElementById("Caramel")
    var Ganache = document.getElementById("Ganache")
    var name = ""
    price = 0
    if (vanilla.checked== true){
        name+=" +Vanilla Flavour"
        price+=+vanilla.value
    }
    if (chocolate.checked== true){
        name+="+ Chocolate Flavour"
        price+=+chocolate.value
    }
    if (Lavender.checked== true){
        name+="+ Lavender Flavour"
        price+=+Lavender.value
    }
    if (rosepetal.checked== true){
        name+="+ Rose-Petal Flavour"
        price+=+rosepetal.value
    }
    if (Butterscotch.checked== true){
        name+="+ Butterscotch Flavour"
        price+=+Butterscotch.value
    }
    if (Lemon.checked== true){
        name+="+ Lemon Flavour"
        price+=+Lemon.value
    }
    if (choco.checked== true){
        name+="+ Choco-Chips Topping"
        price+=+choco.value
    }
    if (Mixedberries.checked== true){
        name+="+ Mixed-Berries Toppings"
        price+=+Mixedberries.value
    }
    if (ButterCream.checked== true){
        name+="+ Butter Cream "
        price+=+ButterCream.value
    }
    if (WhippedCream.checked== true){
        name+="+ Whipped Cream"
        price+=+WhippedCream.value
    }
    if (ItalianMeriangue.checked== true){
        name+="+ Italian Meriangue Cream"
        price+=+ItalianMeriangue.value
    }
    if (Candies.checked== true){
        name+="+ Candies Toppings"
        price+=+Candies.value
    }
    if (Sprinkles.checked== true){
        name+="+ Sprinkles Toppings"
        price+=+Sprinkles.value
    }
    if (FreshFruits.checked== true){
        name+="+ FreshFruits Toppings"
        price+=+FreshFruits.value
    }
    if (RoyalIcing.checked== true){
        name+="+ Royal Icing Cream"
        price+=+RoyalIcing.value
    }
    if (Ganache.checked== true){
        name+="+ Ganache"
        price+=+Ganache.value
    }
    if (Caramel.checked== true){
        name+="+ Caramel"
        price+=+Caramel.value
    }
    if (Frosting.checked== true){
        name+="+ Frosting"
        price+=+Frosting.value
    }
    
    var inputs = document.querySelectorAll('.check');
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].checked = false;
        }
    if (price==0){
        alert("please select atleast one customization option")
    }
    else{
        cakename.innerHTML=""+name;
        totalcost.innerHTML=""+price;
        add(name,price)
        updateCartTotal()
    }
    }
    function add(name, price){
    var cartRow = document.createElement('div')
    cartRow.classList.add('cart-row')
    var cartItems = document.getElementsByClassName('cart-items')[0]
    var cartItemNames = cartItems.getElementsByClassName('cart-item-title')
    for (var i = 0; i < cartItemNames.length; i++) {
        if (cartItemNames[i].innerText == name) {
            alert('This item is already added to the cart')
            return
        }
    }
    var cartRowContents = `
        <div class="cart-item cart-column">
            <img class="cart-item-image" src="straw.jpg" width="100" height="100">
            <span class="cart-item-title">${name}</span>
        </div>
        <span class="cart-price cart-column">${price}</span>
        <div class="cart-quantity cart-column">
            <input class="cart-quantity-input" type="number" value="1">
            <button class="btn btn-danger" type="button">REMOVE</button>
        </div>`
    cartRow.innerHTML = cartRowContents
    cartItems.append(cartRow)
    cartRow.getElementsByClassName('btn-danger')[0].addEventListener('click', removeCartItem)
    cartRow.getElementsByClassName('cart-quantity-input')[0].addEventListener('change', quantityChanged)
}
function removeCartItem(event) {
    var buttonClicked = event.target
    buttonClicked.parentElement.parentElement.remove()
    updateCartTotal()
}

function quantityChanged(event) {
    var input = event.target
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateCartTotal()
}
function updateCartTotal() {
    var cartItemContainer = document.getElementsByClassName('cart-items')[0]
    var cartRows = cartItemContainer.getElementsByClassName('cart-row')
    var total = 0
    for (var i = 0; i < cartRows.length; i++) {
        var cartRow = cartRows[i]
        var priceElement = cartRow.getElementsByClassName('cart-price')[0]
        var quantityElement = cartRow.getElementsByClassName('cart-quantity-input')[0]
        var price = parseFloat(priceElement.innerText.replace('Rs.', ''))
        var quantity = quantityElement.value
        total = total + (price * quantity)
    }
    total = Math.round(total * 100) / 100
    document.getElementsByClassName('cart-total-price')[0].innerText = ' Rs.' + total
}